#!/bin/sh

scribble ++xref-in setup/xref load-collections-xref \
         --redirect-main "https://docs.racket-lang.org/" \
         --dest docs-output \
         ocap-ledger-howto.scrbl 
